netsh interface show interface "Ethernet" |find "Disabled" >nul && (
  echo disabled - enabling...
  netsh interface set interface "Ethernet" enabled
) || (
  echo enabled - disabling
  netsh interface set interface "Ethernet" disabled
)
